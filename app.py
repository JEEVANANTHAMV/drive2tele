from google.colab import drive
import os
from telethon.sync import TelegramClient
from tqdm import tqdm

import asyncio
api_id = '28367018'
api_hash = 'dff8ebd3e6cb937d5a4fc91192262571'

phone_number = '+919360985570'
target_username = '+918098740973'
file_list_path = '/content/drive/My Drive/output_file.txt'

file_paths = read_file_paths(file_list_path)

client = TelegramClient('session_name',api_id,api_hash)

# Function to read file locations from a text file
def read_file_paths(file_path):
    with open(file_path, 'r') as file:
        file_paths = [line.strip() for line in file.readlines()]
    return file_paths

# Function to remove the first line from a text file
def remove_first_line(file_path):
    with open(file_path, 'r') as file:
        lines = file.readlines()
    with open(file_path, 'w') as file:
        file.writelines(lines[1:])

def callback(current, total):
    """
    Args:
      current:
      total:
    """
    if 'pbar' not in callback.__dict__:
        callback.pbar = tqdm(total=total, unit='B', unit_scale=True, desc='Uploading', unit_divisor=1024)
    elif int(total) != int(callback.pbar.total):  # If total changes, update the progress bar
        callback.pbar = tqdm(total=total, unit='B', unit_scale=True, desc='Uploading', unit_divisor=1024)

    callback.pbar.update(current - callback.pbar.n)
    callback.pbar.set_description(f"Uploaded: {current}/{total} bytes")



async def send_file(f):
    async with client as action:
          await client.start(phone=phone_number)
          try:
            await client.send_file(target_username, f,progress_callback=callback)
          except:
            print(f+"not uploaded")

for files in file_paths:
  await send_file(files)
  remove_first_line(file_list_path)
  print(files, file_paths.index(files))